# translation of mumble_1.2.2-4.1_eu.po to Basque
# Copyright (C) 2010
# This file is distributed under the same license as the mumble package.
#
# Piarres Beobide <pi@beobide.net>, 2008.
# Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>, 2010.
msgid ""
msgstr ""
"Project-Id-Version: mumble_1.2.2-4.1_eu\n"
"Report-Msgid-Bugs-To: mumble@packages.debian.org\n"
"POT-Creation-Date: 2010-01-11 16:52+0100\n"
"PO-Revision-Date: 2010-10-14 18:08+0200\n"
"Last-Translator: Iñaki Larrañaga Murgoitio <dooteo@euskalgnu.org>\n"
"Language-Team: Basque <debian-l10n-basque@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid "Password to set on SuperUser account:"
msgstr "Pasahitza 'SuperUser' kontuari ezartzeko:"

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid ""
"Murmur has a special account called \"SuperUser\" which bypasses all "
"privilege checks."
msgstr ""
"Murmur-ek segurtasun egiaztapen guztiak saltatzen dituen \"SuperUser\" "
"deituriko kontu berezi bat du."

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid ""
"If you set a password here, the password for the \"SuperUser\" account will "
"be updated."
msgstr ""
"Pasahitza hemen ezartzen baduzu \"SuperUser\"en pasahitza eguneratu egingo "
"da."

#. Type: password
#. Description
#: ../mumble-server.templates:1001
msgid "If you leave this blank, the password will not be changed."
msgstr "Hutsik uzten baduzu, pasahitza ez da aldatuko."

#. Type: boolean
#. Description
#: ../mumble-server.templates:2001
msgid "Autostart mumble-server on server boot?"
msgstr "Automatikoki abiarazi 'mumble-server' zerbitzaria abioan?"

#. Type: boolean
#. Description
#: ../mumble-server.templates:2001
msgid ""
"Mumble-server (murmurd) can start automatically when the server is booted."
msgstr ""
"Mumble-server (murmurd) automatikoki exekuta daiteke zerbitzaria abiaraztean."

#. Type: boolean
#. Description
#: ../mumble-server.templates:3001
msgid "Allow mumble-server to use higher priority?"
msgstr "Baimendu 'mumble-server'-ek lehentasun handiagoa erabiltzea?"

#. Type: boolean
#. Description
#: ../mumble-server.templates:3001
msgid ""
"Mumble-server (murmurd) can use higher process and network priority to "
"ensure low latency audio forwarding even on highly loaded servers."
msgstr ""
"Mumble-server (murmurd) prozesu eta sareko lehentasun handiagoa erabil "
"dezake audioaren igorpena latentzia baxukoa izatea ziurtatzeko nahiz eta "
"zerbitzariak oso kargatuta egon."
